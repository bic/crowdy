Crowdy - Knowledge from the Crowds
==================================

This project is now hosted at Github, under the name [Infection Map](https://github.com/ic/infectionmap) (the current use of the project).

The project was started at ISAC 2014, continues off line when time allows, and is on its way to ISAC 2015.

